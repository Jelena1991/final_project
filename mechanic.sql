-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Jul 27, 2018 at 08:09 PM
-- Server version: 5.7.19
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mechanic`
--

-- --------------------------------------------------------

--
-- Table structure for table `cars`
--

DROP TABLE IF EXISTS `cars`;
CREATE TABLE IF NOT EXISTS `cars` (
  `car_id` int(11) NOT NULL AUTO_INCREMENT,
  `car_plate` varchar(10) NOT NULL,
  `car_model` varchar(50) NOT NULL,
  `car_brand` varchar(50) NOT NULL,
  `user` int(11) NOT NULL,
  PRIMARY KEY (`car_id`)
) ENGINE=InnoDB AUTO_INCREMENT=136 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cars`
--

INSERT INTO `cars` (`car_id`, `car_plate`, `car_model`, `car_brand`, `user`) VALUES
(1, 'yh789fa', 'bmw', '123', 26),
(2, 'yh789fa', 'grger', 'erfgvesg', 12),
(3, 'yh789fa', 'bmw', 'erfgvesg', 12),
(4, 'yh789fa', 'bmw', 'erfgvesg', 12),
(5, 'yh789fa', 'bmw', 'erfgvesg', 12),
(6, 'yh789fa', 'bmw', 'erfgvesg', 12),
(7, '', '', '', 12),
(8, '', '', '', 12),
(9, '', '', '', 12),
(10, 'yh789fa', 'audi', 'nesto', 12),
(11, 'yh789fa', 'audi', 'nesto', 12),
(12, 'yh789fa', 'audi', 'nesto', 12),
(13, 'yh789fa', 'audi', 'nesto', 12),
(14, 'yhuk4', 'audi', 'erfgvesg', 12),
(15, 'yhuk4', 'audi', 'erfgvesg', 12),
(16, 'yhuk4', 'audi', 'erfgvesg', 12),
(17, '', '', '', 12),
(18, '', '', '', 12),
(19, 'dkdhjwf46', 'bmw', 'erfgvesg', 12),
(20, 'dkdhjwf46', 'bmw', 'erfgvesg', 12),
(21, 'xy47859', 'bmw', 'nesto', 16),
(22, 'dkdhjwf46', 'audi', 'erfgvesg', 16),
(23, 'xzrd456', 'bmw', 'proba', 16),
(24, 'xzrd456', 'bmw', 'proba', 16),
(25, 'xzrd456', 'bmw', 'proba', 16),
(26, 'xzrd456', 'bmw', 'proba', 16),
(27, 'xzrd456', 'bmw', 'proba', 16),
(28, 'dkdhjwf46', 'bmw', 'erfgvesg', 16),
(29, 'dkdhjwf46', 'bmw', 'erfgvesg', 16),
(30, 'dkdhjwf46', 'bmw', 'erfgvesg', 16),
(31, 'dkdhjwf46', 'bmw', 'erfgvesg', 16),
(32, 'dkdhjwf46', 'bmw', 'erfgvesg', 16),
(33, 'dkdhjwf46', 'bmw', 'erfgvesg', 16),
(34, 'dkdhjwf46', 'bmw', 'erfgvesg', 16),
(35, 'xrtgb156', 'fiat', 'PUNTO', 16),
(36, 'hujy489', 'milena', 'milena', 16),
(37, 'hujy489', 'milena', 'milena', 16),
(38, 'hujy489', 'milena', 'milena', 16),
(39, 'hujy489', 'milena', 'milena', 16),
(40, 'hujy489', 'milena', 'milena', 16),
(41, 'hujy489', 'milena', 'milena', 16),
(42, 'hujy489', 'milena', 'milena', 16),
(43, 'hujy489', 'milena', 'milena', 16),
(44, 'hujy489', 'milena', 'milena', 16),
(45, 'dkdhjwf46', 'bmw', 'erfgvesg', 16),
(46, 'dkdhjwf46', 'bmw', 'erfgvesg', 16),
(47, 'dkdhjwf46', 'audi', 'nesto', 16),
(48, 'dkdhjwf46', 'audi', 'nesto', 16),
(49, 'dkdhjwf46', 'audi', 'nesto', 16),
(50, 'dkdhjwf46', 'audi', 'nesto', 16),
(51, 'dkdhjwf46', 'audi', 'nesto', 16),
(52, 'dkdhjwf46', 'bmw', 'erfgvesg', 16),
(53, 'dkdhjwf46', 'bmw', 'erfgvesg', 16),
(54, 'dkdhjwf46', 'bmw', 'erfgvesg', 16),
(55, 'lina123', 'Audi', 'neki', 16),
(56, 'dkdhjwf46', 'audi', 'erfgvesg', 16),
(57, 'dkdhjwf46', 'audi', 'erfgvesg', 16),
(58, 'dkdhjwf46', 'audi', 'erfgvesg', 16),
(59, 'dkdhjwf46', 'audi', 'erfgvesg', 16),
(60, 'dkdhjwf46', 'audi', 'erfgvesg', 16),
(61, 'dkdhjwf46', 'audi', 'erfgvesg', 16),
(62, 'dkdhjwf46', 'audi', 'erfgvesg', 16),
(63, 'dkdhjwf46', 'audi', 'erfgvesg', 16),
(64, 'dkdhjwf46', 'audi', 'erfgvesg', 16),
(65, 'dkdhjwf46', 'audi', 'erfgvesg', 16),
(66, 'dkdhjwf46', 'audi', 'erfgvesg', 16),
(67, 'ghy4789', 'audi', 'nesto', 3),
(68, 'dkdhjwf46', 'audi', 'erfgvesg', 16),
(69, 'xyzgh', 'hdjchnwbsj', 'probaa', 16),
(70, 'xyzgh', 'hdjchnwbsj', 'probaa', 16),
(71, 'dsa', 'dsada', 'dsa', 16),
(72, 'dsa', 'dsada', 'dsa', 16),
(73, 'radi', 'radi', 'radi', 16),
(74, 'radi', 'radi', 'radi', 16),
(75, 'radi', 'radi', 'radi', 16),
(76, 'dkdhjwf46', 'dsada', 'erfgvesg', 16),
(77, 'dkdhjwf46', 'dsada', 'erfgvesg', 16),
(78, 'dkdhjwf46', 'dsada', 'erfgvesg', 16),
(79, 'dkdhjwf46', 'dsada', 'erfgvesg', 16),
(80, 'dsa', 'dsa', 'dsa', 16),
(81, 'dsa', 'dsa', 'dsa', 16),
(82, 'dsa', 'dsa', 'dsa', 16),
(83, 'dsa', 'dsad', 'dsadsa', 16),
(84, 'dsadas', 'asd', 'dsadsa', 16),
(85, 'xy5896', 'audi', 'a689', 16),
(86, 'dfgh7896', 'dfgh7896', 'dfgh7896', 16),
(87, 'probno', 'probno', 'probno', 16),
(88, 'service123', 'service123', 'service123', 16),
(89, 'dkdhjwf46', 'grger', 'nesto', 16),
(90, 'dkdhjwf46', 'dsada', 'erfgvesg', 16),
(91, 'dkdhjwf46', 'grger', 'erfgvesg', 16),
(92, 'dkdhjwf46', 'dsada', 'erfgvesg', 16),
(93, 'dkdhjwf46', 'grger', 'nesto', 16),
(94, 'dkdhjwf46', 'audi', 'erfgvesg', 16),
(95, 'dkdhjwf46', 'audi', 'erfgvesg', 16),
(96, 'dkdhjwf46', 'bmwghv', 'erfgvesg', 16),
(97, 'dkdhjwf46', 'bmwghv', 'erfgvesg', 16),
(98, 'milena', 'milena', 'milena', 16),
(99, 'ytgfu', 'stttyg', 'ghghv', 16),
(100, 'ygi', 'ghfhgvi', 'hgtfuhjv', 16),
(101, 'jelena1', 'linaaa', 'lina', 18),
(102, 'dfdsf', 'hfkahfk', 'dgsdgfv', 16),
(103, 'sgvds', 'yhrjfr', 'sgsdgv', 16),
(104, 'dkdhjwf46', 'bmw', 'erfgvesg', 16),
(105, 'dkdhjwf46', 'audi', '123', 16),
(106, 'dkdhjwf46', 'bmw', 'erfgvesg', 16),
(107, 'frege', 'rfgae', 'ergvedrfs', 16),
(108, 'ergderf', 'regvcdf', 'gerfcg', 16),
(109, 'ewfde', 'wdwfwe', 'wdfsfee', 16),
(110, 'dkdhjwf46', 'grger', 'erfgvesg', 16),
(111, 'dkdhjwf46', 'grger', 'erfgvesg', 16),
(112, 'dkdhjwf46', 'dsada', 'erfgvesg', 16),
(113, 'dkdhjwf46', 'bmw', 'erfgvesg', 16),
(114, 'dkdhjwf46', 'audi', 'erfgvesg', 16),
(115, 'dkdhjwf46', 'bmw', '123', 16),
(116, 'dobrica', 'dobrica', 'dobrice', 16),
(117, 'uros', 'audi', '123', 16),
(118, 'dkdhjwf46', 'bmw', 'nesto', 16),
(119, 'dkdhjwf46', 'audi', 'erfgvesg', 16),
(120, 'dkdhjwf46', 'audi', 'nesto', 16),
(121, 'dkdhjwf46', 'bmw', 'erfgvesg', 16),
(122, 'dkdhjwf46', 'audi', 'erfgvesg', 16),
(123, 'dkdhjwf46', 'bmw', 'erfgvesg', 16),
(124, 'dkdhjwf46', 'audi', 'erfgvesg', 16),
(125, 'dkdhjwf46', 'audi', 'erfgvesg', 16),
(126, 'dkdhjwf46', 'audi', 'nesto', 16),
(127, 'dkdhjwf46', 'audi', 'erfgvesg', 16),
(128, 'dkdhjwf46', 'bmw', 'erfgvesg', 16),
(129, 'dkdhjwf46', 'audi', 'erfgvesg', 16),
(130, 'dkdhjwf46', 'audi', 'erfgvesg', 16),
(131, 'dkdhjwf46', 'audi', 'erfgvesg', 16),
(132, 'dkdhjwf46', 'audi', 'erfgvesg', 16),
(133, 'dkdhjwf46', 'bmw', 'erfgvesg', 16),
(134, 'dkdhjwf46', 'audi', 'erfgvesg', 16),
(135, '132 52', 'mondeo', 'ford', 16);

-- --------------------------------------------------------

--
-- Table structure for table `car_status`
--

DROP TABLE IF EXISTS `car_status`;
CREATE TABLE IF NOT EXISTS `car_status` (
  `car_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `status_car` varchar(20) NOT NULL,
  PRIMARY KEY (`car_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `car_status`
--

INSERT INTO `car_status` (`car_status_id`, `status_car`) VALUES
(1, 'received'),
(2, 'taken'),
(3, 'in progress'),
(4, 'problem solved'),
(5, 'rejected');

-- --------------------------------------------------------

--
-- Table structure for table `problems`
--

DROP TABLE IF EXISTS `problems`;
CREATE TABLE IF NOT EXISTS `problems` (
  `problem_id` int(11) NOT NULL AUTO_INCREMENT,
  `service` int(11) NOT NULL,
  `service_report` int(11) NOT NULL,
  `service_final_price` int(11) NOT NULL,
  `worker_note` varchar(250) NOT NULL DEFAULT 'no comment',
  PRIMARY KEY (`problem_id`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `problems`
--

INSERT INTO `problems` (`problem_id`, `service`, `service_report`, `service_final_price`, `worker_note`) VALUES
(1, 1, 7, 254, 'no comment'),
(2, 2, 7, 33, 'no comment'),
(3, 2, 5, 589, 'no comment'),
(4, 3, 5, 365, 'no comment'),
(5, 1, 18, 256, 'no comment'),
(6, 2, 18, 256, 'no comment'),
(7, 1, 19, 256, 'no comment'),
(8, 2, 19, 256, 'no comment'),
(9, 1, 20, 145, 'no comment'),
(10, 2, 20, 145, 'no comment'),
(11, 3, 20, 145, 'no comment'),
(12, 4, 20, 145, 'no comment'),
(13, 1, 21, 145, 'no comment'),
(14, 3, 21, 145, 'no comment'),
(15, 4, 21, 145, 'no comment'),
(16, 1, 32, 145, 'no comment'),
(17, 2, 32, 145, 'no comment'),
(18, 3, 32, 145, 'no comment'),
(19, 4, 32, 145, 'no comment'),
(20, 1, 33, 200, 'no comment'),
(21, 2, 33, 256, 'no comment'),
(22, 2, 34, 256, 'no comment'),
(23, 4, 34, 145, 'no comment'),
(24, 1, 35, 200, 'no comment'),
(25, 2, 35, 256, 'no comment'),
(26, 1, 36, 0, 'no comment'),
(27, 2, 36, 0, 'no comment'),
(28, 3, 36, 0, 'no comment'),
(29, 1, 37, 0, 'no comment'),
(30, 3, 37, 0, 'no comment'),
(31, 1, 38, 0, 'no comment'),
(32, 2, 38, 0, 'no comment'),
(33, 1, 39, 0, 'no comment'),
(34, 2, 39, 0, 'no comment'),
(35, 3, 39, 0, 'no comment'),
(36, 1, 40, 0, 'no comment'),
(37, 2, 40, 0, 'no comment'),
(38, 3, 40, 0, 'no comment'),
(39, 4, 40, 0, 'no comment'),
(40, 1, 41, 0, 'no comment'),
(41, 2, 41, 0, 'no comment'),
(42, 3, 41, 0, 'no comment'),
(43, 1, 42, 200, 'no comment'),
(44, 2, 42, 256, 'no comment'),
(45, 3, 42, 789, 'no comment'),
(46, 1, 43, 200, 'no comment'),
(47, 2, 43, 256, 'no comment'),
(48, 3, 43, 789, 'no comment'),
(49, 1, 44, 200, 'no comment'),
(50, 2, 44, 256, 'no comment'),
(51, 3, 44, 789, 'no comment'),
(52, 1, 45, 200, 'no comment'),
(53, 2, 45, 256, 'no comment'),
(54, 3, 45, 789, 'no comment'),
(55, 1, 46, 200, 'no comment'),
(56, 3, 46, 789, 'no comment'),
(57, 1, 47, 200, 'no comment'),
(58, 2, 47, 256, 'no comment'),
(59, 3, 47, 789, 'no comment'),
(60, 30, 48, 0, 'no comment'),
(61, 56, 48, 0, 'no comment'),
(62, 78, 48, 0, 'no comment'),
(63, 120, 48, 0, 'no comment'),
(64, 1, 49, 200, 'no comment'),
(65, 2, 49, 256, 'no comment'),
(66, 3, 49, 789, 'no comment'),
(67, 1, 50, 200, 'no comment'),
(68, 2, 50, 256, 'no comment'),
(69, 3, 50, 789, 'no comment'),
(70, 1, 51, 0, 'no comment'),
(71, 2, 51, 0, 'no comment'),
(72, 3, 51, 0, 'no comment'),
(73, 4, 51, 0, 'no comment'),
(74, 1, 52, 0, 'no comment'),
(75, 2, 52, 0, 'no comment'),
(76, 3, 52, 0, 'no comment'),
(77, 1, 53, 0, 'no comment'),
(78, 2, 53, 0, 'no comment'),
(79, 3, 53, 0, 'no comment'),
(80, 1, 54, 0, 'no comment'),
(81, 2, 54, 0, 'no comment'),
(82, 2, 55, 0, 'no comment'),
(83, 3, 55, 0, 'no comment'),
(84, 4, 55, 0, 'no comment'),
(85, 1, 56, 0, 'no comment'),
(86, 2, 56, 0, 'no comment'),
(87, 3, 56, 0, 'no comment'),
(88, 1, 57, 0, 'no comment'),
(89, 2, 57, 0, 'no comment'),
(90, 3, 57, 0, 'no comment'),
(91, 4, 57, 0, 'no comment'),
(92, 5, 57, 0, 'no comment'),
(93, 6, 57, 0, 'no comment'),
(94, 7, 57, 0, 'no comment'),
(95, 8, 57, 0, 'no comment');

-- --------------------------------------------------------

--
-- Table structure for table `report_status`
--

DROP TABLE IF EXISTS `report_status`;
CREATE TABLE IF NOT EXISTS `report_status` (
  `report_status_id` int(11) NOT NULL AUTO_INCREMENT,
  `report_status` varchar(11) NOT NULL,
  PRIMARY KEY (`report_status_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `report_status`
--

INSERT INTO `report_status` (`report_status_id`, `report_status`) VALUES
(1, 'accepted'),
(2, 'reject'),
(3, 'pending');

-- --------------------------------------------------------

--
-- Table structure for table `reservation`
--

DROP TABLE IF EXISTS `reservation`;
CREATE TABLE IF NOT EXISTS `reservation` (
  `reservation_id` int(5) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `duration` int(6) NOT NULL,
  PRIMARY KEY (`reservation_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reservation`
--

INSERT INTO `reservation` (`reservation_id`, `date`, `duration`) VALUES
(1, '2018-07-30', 3000),
(2, '2018-07-30', 432),
(3, '2018-08-02', 1),
(4, '2018-08-22', 106),
(5, '2018-07-27', 264);

-- --------------------------------------------------------

--
-- Table structure for table `role_key`
--

DROP TABLE IF EXISTS `role_key`;
CREATE TABLE IF NOT EXISTS `role_key` (
  `role_key_id` int(5) NOT NULL AUTO_INCREMENT,
  `role_key` varchar(20) NOT NULL,
  PRIMARY KEY (`role_key_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role_key`
--

INSERT INTO `role_key` (`role_key_id`, `role_key`) VALUES
(1, 'admin'),
(2, 'user');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
CREATE TABLE IF NOT EXISTS `services` (
  `service_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_name` varchar(200) NOT NULL,
  `service_description` varchar(300) NOT NULL,
  `service_price` decimal(10,0) NOT NULL,
  `service_duration` int(10) NOT NULL,
  `service_image` varchar(50) NOT NULL,
  PRIMARY KEY (`service_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`service_id`, `service_name`, `service_description`, `service_price`, `service_duration`, `service_image`) VALUES
(1, 'Change the engine oil', 'Motor oil is any of various substances comprising base oils enhanced with additives, particularly antiwear additive plus detergents, dispersants and, index improvers. The main function of motor oil is to reduce friction and wear on moving parts and to clean the engine from sludge and varnish. ', '200', 20, 'image.jpg'),
(2, 'Replace the oil filter', 'An oil filter is a filter designed to remove contaminants from engine oil, transmission oil, lubricating oil, or hydraulic oil. Oil filters are used in many different types of hydraulic machinery. Other vehicle hydraulic systems, are often equipped with an oil filter. ', '256', 30, 'image1'),
(3, 'Replace the air filter', 'A particulate air filter is a device composed of fibrous or porous materials which remove sol particulates such as dust, pollen, mold, and bacterias. Filters containing an absorbent or catalyst such as charcoal may also remove odors and gaseous pollutants. Oil bath filters have fallen out of favor. ', '789', 56, 'image2.jpg'),
(4, 'Replace the fuel filter', 'A fuel filter is a filter in the fuel line that screens out dirt and dust particles from the fuel, normally made into cartridges containing a filter paper.  If these substances are not removed before the fuel enters the system, they will cause rapid wear and failure of the fuel pump and injectors.', '145', 78, 'tyfyt.jpg'),
(5, 'Check level and refill power steering fluid', 'A hydraulic fluid or hydraulic liquid is the medium by which power is transferred in hydraulic machinery. Common hydraulic fluids are based on mineral oil or water. Examples of equipment that might use hydraulic fluids are excavators and backhoes, hydraulic brakes, power steering systems, etc.', '200', 20, 'image.jpg'),
(6, 'Tune the engine', 'An engine or motor is a machine designed to convert one form of energy into mechanical energy. Electric motors convert electrical energy into mechanical motion; pneumatic motors use compressed air, and clockwork motors in wind-up toys use elastic energy.', '200', 20, 'image.jpg'),
(7, 'Replace the spark plugs', 'A spark plug is a device for delivering electric current from an ignition system to the combustion chamber of a spark-ignition engine to ignite the compressed fuel/air mixture by a spark while containing combustion pressure within the engine.It has a metal shell, isolated from a central electrode. ', '200', 20, 'image.jpg'),
(8, 'Inspect and replace the timing belt or timing chain if needed', 'A timing belt is a part of an internal combustion engine that synchronizes the rotation of the crankshaft and the camshaft(s) so that the engine\'s valves open and closes at the proper times during each cylinder\'s intake and exhaust strokes. ', '200', 20, 'image.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `service_report`
--

DROP TABLE IF EXISTS `service_report`;
CREATE TABLE IF NOT EXISTS `service_report` (
  `report_id` int(5) NOT NULL AUTO_INCREMENT,
  `user` int(5) NOT NULL,
  `reservation` int(5) NOT NULL,
  `report_status` int(5) NOT NULL DEFAULT '3',
  `report_car` int(5) NOT NULL,
  `car_status` int(5) NOT NULL DEFAULT '0',
  `final_price` decimal(65,0) NOT NULL,
  `final_time` int(5) NOT NULL DEFAULT '0',
  `worker_message` varchar(500) NOT NULL,
  `user_comment` varchar(500) NOT NULL,
  `notification` varchar(5) NOT NULL DEFAULT 'no',
  PRIMARY KEY (`report_id`),
  KEY `reservation` (`reservation`),
  KEY `report_status` (`report_status`,`report_car`,`car_status`),
  KEY `report_car` (`report_car`),
  KEY `car_status` (`car_status`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `service_report`
--

INSERT INTO `service_report` (`report_id`, `user`, `reservation`, `report_status`, `report_car`, `car_status`, `final_price`, `final_time`, `worker_message`, `user_comment`, `notification`) VALUES
(1, 16, 1, 2, 1, 1, '200', 0, 'ovo je probni page', 'KSASKJJKXBKJBJKSBKA', 'radi'),
(2, 17, 256, 3, 6, 2, '450', 0, 'djwnjkedj', 'NEKI USER KOMENTAR O PROBLEMU', 'no'),
(3, 1, 15, 3, 2, 2, '214', 0, 'grgggerg', 'jhfnhdcns', 'no'),
(4, 16, 4564, 3, 12, 4, '214', 0, 'grgggerg', 'jhfnhdcns', 'no'),
(5, 16, 4564, 1, 12, 5, '21', 23, 'dfg', 'jhfnhdcns', 'no'),
(6, 16, 123, 3, 1, 0, '214', 0, 'grgggerg', 'jhfnhdcns', 'no'),
(7, 16, 123, 3, 1, 0, '214', 0, 'grgggerg', 'jhfnhdcns', 'no'),
(8, 16, 456, 3, 55, 3, '56', 0, 'derfw', 'efwf', 'no'),
(9, 16, 123, 3, 1, 0, '214', 0, 'grgggerg', 'jhfnhdcns', 'no'),
(10, 16, 1, 2, 67, 2, '185', 0, 'gsgsg', 'ggsg', 'no'),
(11, 16, 1, 3, 71, 0, '214', 0, 'grgggerg', 'jhfnhdcns', 'no'),
(12, 16, 1, 3, 73, 0, '214', 0, 'grgggerg', 'jhfnhdcns', 'no'),
(13, 16, 1, 3, 76, 0, '214', 0, 'grgggerg', 'jhfnhdcns', 'no'),
(14, 16, 1, 3, 80, 0, '214', 0, 'grgggerg', 'jhfnhdcns', 'no'),
(15, 16, 1, 3, 81, 0, '214', 0, 'grgggerg', 'jhfnhdcns', 'no'),
(16, 16, 1, 3, 82, 0, '214', 0, 'grgggerg', 'jhfnhdcns', 'no'),
(17, 16, 1, 3, 83, 0, '214', 0, 'grgggerg', 'jhfnhdcns', 'no'),
(18, 16, 1, 1, 84, 0, '456', 0, 'grgggerg', 'jhfnhdcns', 'no'),
(19, 16, 1, 1, 85, 0, '456', 0, 'grgggerg', 'jhfnhdcns', 'no'),
(20, 16, 1, 1, 86, 0, '1390', 0, 'grgggerg', 'probno probno', 'no'),
(21, 16, 1, 1, 87, 0, '1134', 0, 'grgggerg', 'probno', 'no'),
(22, 16, 1, 3, 88, 0, '214', 0, 'grgggerg', 'service123', 'no'),
(23, 16, 1, 3, 89, 0, '214', 0, 'grgggerg', 'wecfcfdefc', 'no'),
(24, 16, 1, 3, 90, 0, '214', 0, 'grgggerg', 'wecfcfdefc', 'no'),
(25, 16, 1, 3, 91, 0, '214', 0, 'grgggerg', 'wecfcfdefc', 'no'),
(26, 16, 1, 3, 92, 0, '214', 0, 'grgggerg', 'wecfcfdefc', 'no'),
(27, 16, 1, 3, 93, 0, '214', 0, 'grgggerg', 'wecfcfdefc', 'no'),
(28, 16, 1, 3, 94, 0, '214', 0, 'grgggerg', 'wecfcfdefc', 'no'),
(29, 16, 1, 3, 95, 0, '214', 0, 'grgggerg', 'wecfcfdefc', 'no'),
(30, 16, 1, 3, 96, 0, '214', 0, 'grgggerg', 'wecfcfdefc', 'no'),
(31, 16, 1, 3, 97, 0, '214', 0, 'grgggerg', 'wecfcfdefc', 'no'),
(32, 16, 1, 1, 98, 0, '1390', 0, 'grgggerg', 'milena', 'no'),
(33, 16, 1, 1, 99, 0, '456', 0, 'grgggerg', 'gcvfvii', 'no'),
(34, 16, 1, 1, 100, 0, '401', 0, 'grgggerg', 'ugi', 'no'),
(35, 18, 1, 1, 101, 0, '456', 0, 'grgggerg', 'fgsgs', 'no'),
(36, 16, 1, 2, 102, 0, '214', 0, 'grgggerg', 'dfaef', 'no'),
(37, 16, 1, 2, 103, 0, '214', 0, 'grgggerg', 'sdgf', 'no'),
(38, 16, 1, 2, 104, 0, '214', 0, 'grgggerg', 'wecfcfdefc', 'no'),
(39, 16, 1, 2, 105, 0, '214', 0, 'grgggerg', 'wecfcfdefc', 'no'),
(40, 16, 1, 2, 106, 0, '214', 0, 'grgggerg', 'wecfcfdefc', 'no'),
(41, 16, 1, 2, 107, 0, '214', 0, 'grgggerg', 'esgerfgergr', 'no'),
(42, 16, 1, 1, 108, 0, '1245', 0, 'grgggerg', 'erfgbvrfde', 'no'),
(43, 16, 1, 1, 109, 0, '1245', 0, 'grgggerg', 'wefwe', 'no'),
(44, 16, 1, 1, 110, 0, '1245', 0, 'grgggerg', 'wecfcfdefc', 'no'),
(45, 16, 1, 1, 111, 0, '1245', 106, 'grgggerg', 'probno probno', 'no'),
(46, 16, 1, 1, 112, 0, '100', 241, '896', 'wecfcfdefc', 'no'),
(47, 16, 1, 1, 113, 0, '1245', 106, 'grgggerg', 'linaaaaa', 'no'),
(49, 16, 1, 1, 115, 0, '1245', 106, 'grgggerg', 'wecfcfdefc', 'no'),
(50, 16, 1, 1, 116, 1, '1245', 106, 'grgggerg', 'dobrica', 'no'),
(51, 16, 1, 2, 117, 0, '0', 0, 'grgggerg', 'jdfkij', 'no'),
(52, 16, 1, 2, 118, 0, '0', 0, 'grgggerg', 'wecfcfdefc', 'no'),
(53, 16, 1, 2, 119, 0, '0', 0, 'grgggerg', 'wecfcfdefc', 'no'),
(54, 16, 1, 2, 120, 0, '0', 0, 'grgggerg', 'wecfcfdefc', 'no'),
(55, 16, 3, 3, 133, 0, '0', 0, 'grgggerg', 'esgvcsde', 'yes'),
(56, 16, 4, 3, 134, 0, '0', 0, 'grgggerg', 'wecfcfdefc', 'yes'),
(57, 16, 5, 3, 135, 0, '0', 0, 'grgggerg', 'dobrica', 'yes');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` text NOT NULL,
  `validation_code` text NOT NULL,
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `admin_key` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `email`, `username`, `password`, `validation_code`, `active`, `admin_key`) VALUES
(15, 'Car', 'Service', 'carservice123ad@gmail.com', ' carservice', '9839492deb4404e595b187c05aa5532a', 'f243a8c626bcecbf28c2c253f2b25097', 1, 2),
(16, 'maja', 'maja', 'maja1@gmail.com', ' maja', '202cb962ac59075b964b07152d234b70', '0', 1, 1),
(17, 'milena', 'miletic', 'milena.miletic@activecollab.com', ' mimi', '984987cea0751a4a055ef0999c19ed6d', '04c74c270070b6a91b25c95964a382eb', 1, 1),
(18, 'lina', 'lina', 'lina@gmail.com', ' lina', '68053af2923e00204c3ca7c6a3150cf7', '0', 1, 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
