<?php


/******** helper functions ******/
function carplate_exists ($car_plate) {
    $sql = "SELECT id FROM cars WHERE car_plate = '$car_plate'";

    $result = query($sql);
    if(row_count($result)==1) {
        return true;
    }
    else {
        return false;
    }
}

/*******validate user report car *******/
function validate_user_report(){

    $errors = []; //empty array za greske

    $min = 3;
    $max = 10;

    if($_SERVER['REQUEST_METHOD'] == "POST"){
        $model = clean($_POST['model']);
        $brend = clean($_POST['brend']);
        $car_plate = clean($_POST['carplate']);
        $reservation = clean($_POST['reservation']);
        $description = clean($_POST['description']);
        

        if(empty($model)){
            //proverava da li je prazno polje
            $errors[] = "Model car cannot be empty.";
        }

        if(strlen($model) < $min){
            //minimalna duzina imena
            $errors[] = "Model car cannot be less than {$min} characters";
        }

        if(strlen($model) > $max){
            //minimalna duzina imena
            $errors[] = "Model car cannot be more than {$max} characters";
        }

        if(empty($brend)){
            //proverava da li je prazno polje
            $errors[] = "Brend car cannot be empty.";
        }

        if(strlen($brend) < $min){
            //minimalna duzina imena
            $errors[] = "Brend car cannot be less than {$min} characters";
        }

        if(strlen($brend) > $max){
            //minimalna duzina imena
            $errors[] = "Brend car cannot be more than {$max} characters";
        }

        if(empty($car_plate)){
            //proverava da li je prazno polje
            $errors[] = "Car plate cannot be empty.";
        }

        if(strlen($car_plate) < $min){
            //minimalna duzina imena
            $errors[] = "Car plate cannot be less than {$min} characters";
        }

        if(strlen($car_plate) > $max){
            //minimalna duzina imena
            $errors[] = "Car plate cannot be more than {$max} characters";
        } 



        if(empty($description)){
            //proverava da li je prazno polje
            $errors[] = "Description cannot be empty.";
        }


        if(strlen($description) < $min){
            //minimalna duzina imena
            $errors[] = "Description cannot be less than {$min} characters";
        } 


        if(!empty($errors)) {
            foreach ($errors as $error) {
                // echo '<p class="error" style="border:1px solid red; background-color: rgb(240, 157, 157); color: red; font-weight: bold">'.$error.'</p>';
                //bootstrap za lepse ispisivanje greske 
                echo validation_errors($error);
                }
            }
        else{

            if (report_problem($model, $brend, $car_plate, $reservation, $description)) {
                set_message ('<p style="border:1px solid rgb(30, 131, 5); background-color: rgb(186, 250, 144); color: rgb(30, 131, 5); font-weight: bold">Vasa prijava je prosledjena</p>');

                redirect("user.php");

            }
            else {
                set_message ('<p style="border:1px solid rgb(30, 131, 5); background-color: rgb(186, 250, 144); color: rgb(60, 0, 83); font-weight: bold">Sorry, something went wrong</p>');

                redirect("user.php");
            }

        }
        
    } //post request



} //function
function report_problem($model, $brend, $car_plate, $reservation, $description) {
    $user_id=$_SESSION["userid"];
    $model = escape($model);
    $brend = escape($brend);
    $car_plate = escape($car_plate);
    $reservation = escape($reservation);
    $description = escape($description);

    // escaping prevent sql injection

    if(!carplate_exists ($car_plate)){
        $sql = "INSERT INTO cars(car_plate, car_model, car_brand, user) VALUES ('$car_plate', '$model', '$brend', ' $user_id')";
        $result = query($sql);
        confirm($result);
        $car_id = get_last_inserted_id();
        // $car_id = $result['car_id'];
    }

    else {
        $sql = "SELECT car_id FROM cars WHERE car_plate = '$car_plate'";
        $result = query($sql);
        confirm($result);

        $car_id = $result['car_id'];
    }

    $sql2 = "INSERT INTO service_report(user, reservation, report_car, final_price, worker_message, user_comment, notification) VALUES ('$user_id', '1', '$car_id', '0', 'grgggerg', '$description', 'yes')";
    $result2 = query($sql2);
    $service_report_id = get_last_inserted_id();
    confirm($result2);


        if(!empty($_POST['cars'])) {
            // Loop to store and display values of individual checked checkbox.
            
            
            foreach($_POST['cars'] as $service) {
                var_dump($service_price);     
                $sql_serv = "INSERT INTO problems(service, service_report, service_final_price) VALUES ($service, $service_report_id, '0')";

                $result_serv = query($sql_serv);
                confirm($result_serv);
      
                }
            }
        else{
            echo "<b>Please Select Atleast One Option.</b>";
        }
}

/****** select services functions *****/

function select_services(){
    $sql = "SELECT * FROM services";
    $results =query($sql);
    // var_dump($results);
    
    $row = fetch_array($results);
    //var_dump ($row);

    foreach ($results as $result) {
      
        $service_id = $result['service_id'];
        $service_name = $result['service_name'];
        $service_price = $result['service_price'];
        $service_duration = $result['service_duration'];
        echo "<input type='checkbox'  name='cars[]' value='";
        echo $service_id;
        echo "'>";
        echo $service_name;
        echo ", ";
        echo $service_price;
        echo ", ";
        echo $service_duration;
        echo "minutes. <br>";
    }
}


/***** check car */








?>
