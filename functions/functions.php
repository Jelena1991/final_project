<?php
/** CONSTANTS */
const ADMIN_ROLE_ID = 2;
const USER_ROLE_ID = 1;

// ***********************helper functions***********************/
function clean($string){
    return htmlentities($string);
}

function redirect($location){
    return header("Location: {$location}");

}

// funkcija koja stavlja poruke u session i mozemo da vidimo poruke u svim delovima sajta

function set_message($message){

    if(!empty($message)){

        $_SESSION['message'] = $message;

    }
    else {
        $message = "";
    }

}

function display_message(){
    if(isset($_SESSION['message'])){
        echo $_SESSION['message'];
        unset($_SESSION['message']);
    }
}


function token_generator(){
    $token = $_SESSION['token'] = md5(uniqid(mt_rand(), true));
    return $token;
    //uniqid-jedinstveni broj, mt_rand neke random vrednosti
}




function validation_errors($error_message) {
$error_message = <<<DELIMITHER
<p class="error" style="border:1px solid red; background-color: rgb(240, 157, 157); color: red; font-weight: bold">$error_message</p>
DELIMITHER;
return $error_message;
}

function email_exists ($email) {
    $sql = "SELECT id FROM users WHERE email = '$email'";

    $result = query($sql);
    if(row_count($result)==1) {

        return true;
    }
    else {
        return false;
    }
}

function username_exists ($username) {
    $sql = "SELECT id FROM users WHERE username = '$username'";

    $result = query($sql);
    if(row_count($result)==1) {
        return true;
    }
    else {
        return false;
    }
}


function send_email($email, $subject, $msg, $headers){

 return mail ($email, $subject, $msg, $headers);
    

}


// ***********************Validation functions***********************/

function validate_user_registration(){

    $errors = []; //empty array za greske

    $min = 3;
    $max = 20;

    if($_SERVER['REQUEST_METHOD'] == "POST"){
        
        $first_name = clean($_POST['first_name']);
        $last_name = clean($_POST['last_name']);
        $username = clean($_POST['username']);
        $email = clean($_POST['email']);
        $password = clean($_POST['password']);
        $confirm_password = clean($_POST['confirm_password']);

        if(empty($first_name)){
            //proverava da li je prazno polje
            $errors[] = "Your first name cannot be empty.";
        }

        if(strlen($first_name) < $min){
            //minimalna duzina imena
            $errors[] = "Your first name cannot be less than {$min} characters";
        }

        if(strlen($first_name) > $max){
            //minimalna duzina imena
            $errors[] = "Your first name cannot be more than {$max} characters";
        }

        if(empty($last_name)){
            //proverava da li je prazno polje
            $errors[] = "Your last name cannot be empty.";
        }

        if(strlen($last_name) < $min){
            //minimalna duzina imena
            $errors[] = "Your last name cannot be less than {$min} characters";
        }

        if(strlen($last_name) > $max){
            //minimalna duzina imena
            $errors[] = "Your last name cannot be more than {$max} characters";
        }

        if(empty($username)){
            //proverava da li je prazno polje
            $errors[] = "Your username cannot be empty.";
        }

        if(strlen($username) < $min){
            //minimalna duzina imena
            $errors[] = "Your username cannot be less than {$min} characters";
        }

        if(strlen($username) > $max){
            //minimalna duzina imena
            $errors[] = "Your username cannot be more than {$max} characters";
        } 

        if(username_exists ($username)){
            $errors[] = "Sory that username already is taken";
        }

        if(empty($email)){
            //proverava da li je prazno polje
            $errors[] = "Your email cannot be empty.";
        }


        if(email_exists ($email)){
            $errors[] = "Sory that email already is registered";
        }

        if(strlen($email) < $min){
            //minimalna duzina imena
            $errors[] = "Your email cannot be less than {$min} characters";
        } 

        if($password !== $confirm_password ) {
            $errors[] = "Your password fields do not match";
        }



        if(!empty($errors)) {
            foreach ($errors as $error) {
                // echo '<p class="error" style="border:1px solid red; background-color: rgb(240, 157, 157); color: red; font-weight: bold">'.$error.'</p>';
                //bootstrap za lepse ispisivanje greske 
                echo validation_errors($error);
                }
            }
        else{

            if (register_user($first_name, $last_name, $username, $email, $password)) {
                set_message ('<p style="border:1px solid rgb(30, 131, 5); background-color: rgb(186, 250, 144); color: rgb(30, 131, 5); font-weight: bold">Please check your email or spam folder for activation link</p>');

                redirect("index.php");

            }
            else {
                set_message ('<p style="border:1px solid rgb(30, 131, 5); background-color: rgb(186, 250, 144); color: rgb(60, 0, 83); font-weight: bold">Sorry we could not register the user</p>');

                redirect("index.php");
            }

        }
        
    } //post request



} //function

//Register user functions

function register_user($first_name, $last_name, $username, $email, $password) {

    $first_name = escape($first_name);
    $last_name = escape($last_name);
    $username = escape($username);
    $email = escape($email);
    $password = escape($password);
    // escaping prevent sql injection

    if(email_exists($email)) {
        return false;
    }

    else if (username_exists($username)){
        return false;
    }
    else {
        $password = md5($password); //ovo mogu doraditi, sigurnost sifre
        $validation_code = md5($username . microtime());
        $sql = "INSERT INTO users(first_name, last_name, email, username, password, validation_code, active) VALUES ('$first_name', '$last_name', '$email', ' $username', '$password', '$validation_code', '0')";
        $result = query($sql);
        confirm($result);

        $subject = "Activate Account";
        $msg = " Please click link below to activate your Account
        http://repaircar.startrek.rs/activate.php?email=$email&code=$validation_code
        ";

        $headers = "From: cerservice123ad.gmail.com";



        send_email($email, $subject, $msg, $headers);


        return true;
    }
}


//Activate user functions

function activate_user() {
    if($_SERVER['REQUEST_METHOD']== "GET") {
        
        if(isset($_GET['email'])) {
            $email = clean(($_GET['email']));
            $validation_code = clean(($_GET['code']));

            $sql = "SELECT id FROM users WHERE email ='".escape($_GET['email'])."' AND validation_code = '".escape($_GET['code'])."' ";

            
            $result = query($sql);
            confirm($result);

            if(row_count($result) == 1){
                $sql2 = "UPDATE users SET active = 1, validation_code = 0 WHERE email = '".escape($email)."' AND validation_code = '".escape($validation_code)."' ";
                $result2 = query($sql2);
                confirm($result2);

                set_message ('<p style="border:1px solid rgb(104, 10, 99); background-color: rgb(229, 179, 248); color: rgb(104, 10, 99); font-weight: bold">Your account has been activated.Please log in</p>');
                redirect("login.php");
            }
            else {
                set_message ('<p style="border:1px solid rgb(104, 10, 99); background-color: red; color: rgb(104, 10, 99); font-weight: bold">Sorry. Your account could not been activated.</p>');
                redirect("login.php");
            }
        }
        
    }
} // function

/*****************validate user login  ***********/

function validate_user_login(){

    $errors = []; //empty array za greske

    $min = 3;
    $max = 20;

    if($_SERVER['REQUEST_METHOD'] == "POST"){
        $email = clean($_POST['email']);
        $password = clean($_POST['password']);
        $remember = isset($_POST['remember']);




        if(empty($email)){
            //proverava da li je prazno polje
            $errors[] = "Your email cannot be empty.";
        }

        if(empty($password)){
            //proverava da li je prazno polje
            $errors[] = "Your password cannot be empty.";
        }
        //treba jos provera ovde - ovde je samo za prazno polje 
        
        if(!empty($errors)) {
            foreach ($errors as $error) {
                // echo '<p class="error" style="border:1px solid red; background-color: rgb(240, 157, 157); color: red; font-weight: bold">'.$error.'</p>';
                //bootstrap za lepse ispisivanje greske 
                echo validation_errors($error);
                }
            }
        else {
            // if login_admin.. jos kao jedan uslov
            if(login_user($email, $password, $remember)){
                
                redirect("index.php");
            }

            if(login_admin($email, $password, $remember)){
                
                redirect("index.php");
            }

           
            else {
                echo validation_errors("Email or password are not correct");
            }

        }

    }

} // function


/************ user login functions ************/

    function login_user ($email, $password, $remember) {
        $sql = "SELECT password, id, admin_key FROM users WHERE email = '".escape($email)."' AND active=1";
        $result =query($sql);

        if(row_count($result)==1) {
            $row = fetch_array($result);
            $db_password = $row['password'];
            $_SESSION["userid"] = trim($row["id"]);
            $_SESSION["rolekey"] = trim($row["adminkey"]);

            if(md5($password) === $db_password) {
                if($remember == "on") {
                    setcookie('email', $email, time() + 86400);
                }
                $_SESSION['email'] = $email;
                return true;
            }
            else {
                return false;
            }
            return true;
        }
        else {
            return false;
        }

    } // end of function


function admin_user(){
    if (logged_in()){
    $user_id=$_SESSION["userid"];
    $sql = "SELECT admin_key FROM users WHERE id=$user_id";
    $result =query($sql);
    
        $row = fetch_array($result);
        $role_key = $row['admin_key'];
        
        
        if($role_key == 2) {
            echo "<li><a href='admin.php'>Admin page</a></li>";
        }
        elseif(($role_key == 1)){
            echo "<li><a href='user.php'>User page</a></li>";
        }
        else {
            echo "You are not login, please login";
        }

    }
}


 // functions



/************ admin login functions ************/

    function login_admin ($email, $password, $remember) {
        $sql = "SELECT password, id FROM users WHERE email = '".escape($email)."' AND active=1 AND admin_key=2";
        $result =query($sql);

        if(row_count($result)==1) {
            $row = fetch_array($result);
            $db_password = $row['password'];
            $role_key = $row['admin_key'];


            if(md5($password) === $db_password && $role_key == 2) {
                if($remember == "on") {
                    setcookie('email', $email, time() + 86400);
                }
                $_SESSION['email'] = $email;
                return true;
            }
            else {
                return false;
            }
            return true;
        }
        else {
            return false;
        }

    } // end of function

    /****************** logged in function ***************/

    function logged_in() {
        if(isset($_SESSION['email']) || isset($_COOKIE['email'])){
            

            return true;
        }
        else {
            return false;
        }
    }  // functions

    /********** Recover Password functions*********/

    function recover_password(){
        if($_SERVER['REQUEST_METHOD'] == "POST") {
            if(isset($_SESSION['token']) && $_POST['token'] === $_SESSION['token']){
                $email = clean($_POST['email']);
                if (email_exists($email)) {

                    $validation_code = md5($email . microtime());

                    setcookie('temp_access_code', $validation_code, time()+ 900);

                    $sql = "UPDATE users SET validation_code = '".escape($validation_code)."' WHERE email = '".escape($email)."' ";
                    $result = query($sql);
                    // confirm($result);



                    $subject = "Please reset your password";
                    $message = "Please click the link below to reset your passwword {$validation_code}
                    Click here to reset your pasword http://repaircar.startrek.rs/code.php?email=$email&code=$validation_code;
                
                    ";

                    $headers = "From: noreply@repaircar.startrek.rs"; //ovde zameniti kad dodje liveserver
                    var_dump(send_email($email, $subject, $message, $headers));
                    die("nesto");
                    if (!send_email($email, $subject, $message, $headers)){
                        echo validation_errors ("Email could not be sent");
                    }

                    set_message ("<p>Please check ypur email or spam folder for a password reset code.</p>");
                    redirect("index.php");
        
                } else {
                    echo validation_errors ("This email does not exist");
                }

            } else {
                redirect ("index.php");
            }
            
            
            // token checks
            if(isset($_POST['cancel_submit'])) {
                redirect("login.php");


            }



        } // post request
    } // functions

/************* Code Validation *****************/

function validate_code(){
    if(isset($_COOKIE['temp_access_code'])){
       
        if(!isset($_GET['email']) && !isset($_GET['code'])){
            redirect("index.php"); //sve ovo malo ocistiti
        } 
        else if (empty($_GET['email']) || empty($_GET['code'])){
            redirect("index.php"); //sve ovo malo ocistiti
        }
        else {
            if(isset($_POST['code'])){
                $email = clean($_GET['email']);
                $validation_code = clean($_POST['code']);
                $sql = "SELECT id FROM users WHERE validation_code = '".escape($validation_code)."'AND email = '".escape($email)." '";
                $result = query($sql);

                if(row_count($result) == 1){

                    setcookie('temp_access_code', $validation_code, time()+ 900);
                    redirect("reset.php?email=$email&code=$validation_code");
                }
                else {
                    echo validation_errors("Sorry wrong validation code");
                }

            }
        }
       
    }
    else {
        set_message("<p>Sorry your validation cookie was expire.</p>");
        redirect ("recover.php");
    }
}

/******** Password Reset Function******/

function password_reset() {
    if(isset($_COOKIE['temp_access_code'])){

        if(isset($_GET['email']) && isset($_GET['code'])){

            if(isset($_SESSION['token']) && isset($_POST['token'])) {

                if($_POST['token'] === $_SESSION['token']) {

                    if($_POST['password'] === $_POST['confirm_password']) {

                        $updated_password = md5($_POST['password']);

                        $sql = "UPDATE users SET password = '".escape($updated_password)."', validation_code = 0 WHERE email = '".escape($_GET['email'])."'   ";
                        query($sql);

                        set_message ("<p>Your password has been updated, plesae login</p>");
                        redirect("login.php");

                    }

                    else {
                        echo validation_errors("Password fields don't, match");
                    }

                }
                 
            }
                
        } 
            
    } 
    else {
        set_message("<p>Sorry your time has expired</p>");
        redirect("recover.php");
                
    }

}

/***** USER ADMIN *****/


    




                  





?>