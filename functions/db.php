<?php
define("HOST","localhost");
define("USER","root");
define("PASSWORD","");
define("DATABASE","mechanic");

$con = mysqli_connect(HOST, USER, PASSWORD, DATABASE);

function row_count($result) {
    return mysqli_num_rows($result);
}

function escape($string) {
    global $con;

    return mysqli_real_escape_string($con, $string);
}

function query($query) {
    global $con;
    
    return mysqli_query($con, $query);
}

function get_last_inserted_id() {
    global $con;

    return !empty($con->insert_id) ? $con->insert_id : null;
}

function confirm($result) {
    global $con;
    if(!$result) {
        die("QUERY FAILED" . mysqli_error($con));
    }
}

function fetch_array($result) {
    global $con;
    return mysqli_fetch_array($result);
}


?>