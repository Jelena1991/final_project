<?php  include 'includes/header.php' ?>

	<div class="row">
		<?php display_message(); ?>
		<?php  password_reset(); ?>
	</div>
    	
	<h3>Reset Password</h3>
							
	<form id="register-form" method="post" role="form" >
		<input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password" required>
		<input type="password" name="confirm_password" id="confirm-password" tabindex="2" class="form-control" placeholder="Confirm Password" required>
		<input type="submit" name="reset-password-submit" id="reset-password-submit" tabindex="4" class="form-control btn btn-register" value="Reset Password">

		<input type="hidden" class="hide" name="token" id="token" value="<?php echo token_generator(); ?>">
	</form>
<?php include 'includes/footer.php' ?>