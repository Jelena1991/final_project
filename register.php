<?php  include 'includes/header.php' ?>
<?php  include 'includes/nav.php' ?>


	<div class="row">
		<?php validate_user_registration() ?>	
		<?php display_message(); ?>			
	</div>
    	
	
		<a href="login.php">Login</a>
		<a href="register.php" class="active" id="">Register</a>
							
				
		<form id="register-form" method="post" role="form" >
			<input type="text" name="first_name" id="first_name" tabindex="1" class="form-control" placeholder="First Name" value="" required >
			<input type="text" name="last_name" id="last_name" tabindex="1" class="form-control" placeholder="Last name" value="" required >
			<input type="text" name="username" id="username" tabindex="1" class="form-control" placeholder="Username" value="" required >
			<input type="email" name="email" id="register_email" tabindex="1" class="form-control" placeholder="Email Address" value="" required >
			<input type="password" name="password" id="password" tabindex="2" class="form-control" placeholder="Password" required>
			<input type="password" name="confirm_password" id="confirm-password" tabindex="2" class="form-control" placeholder="Confirm Password" required>
			<input type="text" name="fake_field" hidden>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-sm-6 col-sm-offset-3">
			<input type="submit" name="register-submit" id="register-submit" tabindex="4" class="form-control btn btn-register" value="Register Now">
		</form>
<?php include 'includes/footer.php' ?>