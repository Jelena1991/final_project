
<?php  include 'includes/header.php' ?>

<div>
	<?php recover_password(); ?>
</div>
					
<h2><b>Recover Password</b></h2>
<form id="register-form"  method="post" role="form" autocomplete="off">
	<label for="email">Email Address</label>
	<input type="email" name="email" id="email" tabindex="1" class="form-control" placeholder="Email Address" value="" autocomplete="off" />
										
	<input type="submit" name="cancel_submit" id="cencel-submit" tabindex="2" class="form-control btn btn-danger" value="Cancel" />
												
	<input type="submit" name="recover-submit" id="recover-submit" tabindex="2" class="form-control btn btn-success" value="Send Password Reset Link" />									
										<!-- ovo ne valja vidi se token u inspektu -->
	<input type="hidden" class="hide" name="token" id="token" value="<?php echo token_generator(); ?>">
</form>
<?php include 'includes/footer.php' ?>
