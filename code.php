<?php  include 'includes/header.php' ?>

	<div>
		<div>	
		<?php display_message(); ?>
		<?php validate_code(); ?>					
		</div>
	</div>

    
				
	<div class="text-center"><h2><b> Enter Code</b></h2></div>
	<form id="register-form"  method="post" role="form" autocomplete="off">
		<input type="text" name="code" id="code" placeholder="##########" value="" autocomplete="off" required/>
		<input type="submit" name="code-cancel" id="code-cancel" tabindex="2" class="form-control btn btn-danger" value="Cancel" />
		<input type="submit" name="code-submit" id="recover-submit" tabindex="2" class="form-control btn btn-success" value="Continue" />
								
		<input type="hidden" class="hide" name="token" id="token" value="">
	</form>
<?php include 'includes/footer.php' ?>
